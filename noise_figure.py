import threading
import contextlib
import sys
import time
import statistics
import queue
import bisect

from ldb.hdf5.high_level import HDF5File, HDF5Dataspace, HDF5TypeBuiltin

import alert_thread

import uhd
import numpy
import cmath
import math

print("\033];Noise Figure Measurement\007")

repeat_count = 10

frequencies_subset = [70e6, 1e9, 2e9, 3e9, 4e9, 5e9, 6e9]
frequencies = [70e6] + [100e6*i for i in range(1, 61)]

all_gains = range(77)
gains = [50, 60, 75, 76]


ports = [(0, "TX/RX", "A TX/RX"),
         (0, "RX2", "A RX2"),
         (1, "RX2", "B RX2"),
         (1, "TX/RX", "B TX/RX")]

portmap = {(port[0], port[1]): i for i, port in enumerate(ports)}


try:
    device = uhd.usrp.MultiUSRP()
except RuntimeException as e:
    if e.message == "LookupError: KeyError: No devices found for ----->\nEmpty Device Address":
        print("... engage telepathy ... FAIL ... look for SDR ... FAIL")
        print("Trying plugging in the SDR")
        sys.exit(1)
    else:
        raise e

if device.get_mboard_name() != "B210":
    print("... This is not the radio you are looking for ... FAIL")
    print("Weakminded one")
    sys.exit(1)


mboard_serial_number = device.get_usrp_rx_info()["mboard_serial"]
rx_serial_number = device.get_usrp_rx_info()["rx_serial"]
serial_number = mboard_serial_number + "_" + rx_serial_number
print("Serial number: ", serial_number)

samp_rate = 30.72e6
#samp_rate /= 2

for i in range(2):
    device.set_rx_rate(samp_rate, i)

n_rx = 2**20
n_flush = 1024*2

stream_args = uhd.libpyuhd.usrp.stream_args("fc32", "sc16")
stream_args.channels = [0, 1]

rx_stream = device.get_rx_stream(stream_args)

rx_buff = numpy.zeros((2, n_rx), dtype=numpy.complex64)
rx_tmp_buff = numpy.zeros((2, rx_stream.get_max_num_samps()),
                          dtype=numpy.complex64)

metadata = uhd.libpyuhd.types.rx_metadata()

stream_cmd = uhd.libpyuhd.types.stream_cmd(
    uhd.libpyuhd.types.stream_mode.num_done)
stream_cmd.num_samps = n_rx
stream_cmd.stream_now = False

flush_stream_cmd = uhd.libpyuhd.types.stream_cmd(
    uhd.libpyuhd.types.stream_mode.num_done)
flush_stream_cmd.num_samps = n_flush
flush_stream_cmd.stream_now = False

fail_stream_cmd = uhd.libpyuhd.types.stream_cmd(
    uhd.libpyuhd.types.stream_mode.stop_cont)


noise_low_frequency = 45e3 * 2 / samp_rate


device.set_gpio_attr("FP0", "CTRL", 0, 1, 0)
device.set_gpio_attr("FP0", "DDR", 1, 1, 0)
i = 0


device.set_rx_antenna("TX/RX", 0)
device.set_rx_gain(73, 0)

class ComputationThread(threading.Thread):
    def __init__(self, work_queue, result_queue):
        super().__init__()
        self.work_queue = work_queue
        self.result_queue = result_queue
        self.daemon = True

    def run(self):
        print("Thread running")
        work_item = self.work_queue.get()
        while work_item is not None:
            result_item = numpy.fft.fft(work_item)
            self.result_queue.put(result_item)
            work_item = self.work_queue.get()


work_threads = 0

def measure_noise(frequency, channel, count=10):
#    tune_req = uhd.types.TuneRequest(frequency)
#    device.set_rx_freq(tune_req, channel)
    if work_threads == 0:
        results = []

    work_queues = [queue.Queue(count*2) for _ in range(work_threads)]
    result_queues = [queue.Queue(count*2) for _ in range(work_threads)]

    computation_threads = [ComputationThread(work_queue, result_queue)
                           for work_queue, result_queue
                           in zip(work_queues, result_queues)]
    for computation_thread in computation_threads:
        computation_thread.start()

    noise_figures = []
    noise_power_off_values = []
    noise_power_on_values = []

    queue_idx = 0

    for i in range(count):
        device.set_gpio_attr("FP0", "OUT", 0, 1, 0)

        sdr_time = device.get_time_now().get_real_secs()
        j = 0
        flush_stream_cmd.time_spec = \
                uhd.libpyuhd.types.time_spec(sdr_time + 6e-3)
        rx_stream.issue_stream_cmd(flush_stream_cmd)
        while j < n_flush:
            read_count = rx_stream.recv(rx_tmp_buff, metadata, 0.1)
            if metadata.error_code != \
               uhd.libpyuhd.types.rx_metadata_error_code.none:
                print(j)
            j += read_count


        rx_buff = numpy.zeros((2, n_rx), dtype=numpy.complex64)
        sdr_time = device.get_time_now().get_real_secs()
        j = 0
        stream_cmd.time_spec = \
                uhd.libpyuhd.types.time_spec(sdr_time + 6e-3)
        rx_stream.issue_stream_cmd(stream_cmd)
        while j < n_rx:
            read_count = rx_stream.recv(rx_tmp_buff, metadata, 0.1)
            if metadata.error_code != \
               uhd.libpyuhd.types.rx_metadata_error_code.none:
                # 91800
                print(j)
            rx_buff[:, j:j+read_count] = rx_tmp_buff[:, 0:read_count]
            j += read_count

        if work_threads == 0:
            results.append(numpy.fft.fft(rx_buff[channel, :]))
        else:
            work_queues[queue_idx].put(rx_buff[channel, :])
            queue_idx += 1
            queue_idx %= work_threads


        device.set_gpio_attr("FP0", "OUT", 1, 1, 0)

        sdr_time = device.get_time_now().get_real_secs()
        j = 0
        flush_stream_cmd.time_spec = \
                uhd.libpyuhd.types.time_spec(sdr_time + 6e-3)
        rx_stream.issue_stream_cmd(flush_stream_cmd)
        while j < n_flush:
            read_count = rx_stream.recv(rx_tmp_buff, metadata, 0.1)
            if metadata.error_code != \
               uhd.libpyuhd.types.rx_metadata_error_code.none:
                print(j)
            j += read_count


        rx_buff = numpy.zeros((2, n_rx), dtype=numpy.complex64)
        sdr_time = device.get_time_now().get_real_secs()
        j = 0
        stream_cmd.time_spec = \
                uhd.libpyuhd.types.time_spec(sdr_time + 6e-3)
        rx_stream.issue_stream_cmd(stream_cmd)
        while j < n_rx:
            read_count = rx_stream.recv(rx_tmp_buff, metadata, 0.1)
            if metadata.error_code != \
               uhd.libpyuhd.types.rx_metadata_error_code.none:
                print(j)
            rx_buff[:, j:j+read_count] = rx_tmp_buff[:, 0:read_count]
            j += read_count

        if work_threads == 0:
            results.append(numpy.fft.fft(rx_buff[channel, :]))
        else:
            work_queues[queue_idx].put(rx_buff[channel, :])
            queue_idx += 1
            queue_idx %= work_threads


    for work_queue in work_queues:
        work_queue.put(None)

    queue_idx = 0

    for i in range(count):
        if work_threads == 0:
            noise_spectrum = results.pop(0)
        else:
            noise_spectrum = result_queues[queue_idx].get()
            queue_idx += 1
            queue_idx %= work_threads
        lower_idx = int(noise_low_frequency * len(noise_spectrum))
        noise_power = numpy.sum(noise_spectrum[lower_idx:-lower_idx]*
                                noise_spectrum[lower_idx:-lower_idx].conjugate())
        noise_power_off = noise_power.real / len(noise_spectrum)

        if work_threads == 0:
            noise_spectrum = results.pop(0)
        else:
            noise_spectrum = result_queues[queue_idx].get()
            queue_idx += 1
            queue_idx %= work_threads
        lower_idx = int(noise_low_frequency * len(noise_spectrum))
        noise_power = numpy.sum(noise_spectrum[lower_idx:-lower_idx]*
                                noise_spectrum[lower_idx:-lower_idx].conjugate())
        noise_power_on = noise_power.real / len(noise_spectrum)

        print(noise_power_off, noise_power_on)


        enr_cals = [(0.01e9, 15.15),
                    (0.10e9, 15.01),
                    (1.00e9, 14.93),
                    (2.00e9, 14.45),
                    (3.00e9, 14.28),
                    (4.00e9, 14.26),
                    (5.00e9, 14.27),
                    (6.00e9, 14.27),
                    (7.00e9, 14.31),
                    (8.00e9, 14.44),
                    (9.00e9, 14.58),
                    (10.00e9, 14.58),
                    (11.00e9, 14.54),
                    (12.00e9, 14.45),
                    (13.00e9, 14.32),
                    (14.00e9, 14.14),
                    (15.00e9, 13.91),
                    (16.00e9, 13.71),
                    (17.00e9, 13.54),
                    (18.00e9, 13.45),
                    (19.00e9, 13.71),
                    (20.00e9, 13.81),
                    (21.00e9, 13.85),
                    (22.00e9, 13.86),
                    (23.00e9, 14.10),
                    (24.00e9, 14.41),
                    (25.00e9, 14.62),
                    (26.00e9, 14.58),
                    (26.50e9, 14.47),]
        right_idx = bisect.bisect(enr_cals, (frequency, 0))
        if right_idx == len(enr_cals):
            raise Exception("Out of range, frequency too high")
        if right_idx == 0 and frequency != enr_cals[0][0]:
            raise Exception("Out of range, frequency too low")
        if right_idx == 0:
            enr = enr_cals[right_idx][1]
        else:
            enr_low_frequency, enr_low_value = enr_cals[right_idx-1]
            enr_high_frequency, enr_high_value = enr_cals[right_idx]
            assert enr_low_frequency <= frequency
            assert enr_high_frequency >= frequency
            enr = (enr_low_value * (enr_high_frequency - frequency) /
                   (enr_high_frequency - enr_low_frequency) +
                   enr_high_value * (frequency - enr_low_frequency) /
                   (enr_high_frequency - enr_low_frequency))
        print(frequency, enr, enr_low_value, enr_high_value)

        nt_off = 290
        nt_on = 290 * (1+10**(enr/10))

        m = (noise_power_off - noise_power_on) / (nt_off - nt_on)
        nt_sys = noise_power_off / m - nt_off

        F = (nt_off + nt_sys) / nt_off
        if F <= 0:
            NF = -1
        else:
            NF = 10 * math.log10(F)

        print("NF:", NF)

        noise_figures.append(NF)
        noise_power_off_values.append(noise_power_off)
        noise_power_on_values.append(noise_power_on)

    return noise_figures, noise_power_off_values, noise_power_on_values


while False:
    start = time.time()
    for gain in [60, 65, 70, 75, 76]:
        device.set_rx_gain(gain, 0)
        noise_figures, noise_power_off_values, noise_power_on_values = \
                measure_noise(2.4e9)
        print(noise_figures, noise_power_off_values, noise_power_on_values)
        print(statistics.mean(noise_figures), statistics.stdev(noise_figures))

    print(time.time() - start)

    if input() == 'q':
        quit()


with contextlib.ExitStack() as context_stack:
    h5_file = context_stack.enter_context(
        HDF5File.open("nf_%s.h5"%(serial_number)))

    main_results_dataspace = context_stack.enter_context(
        HDF5Dataspace.create_simple(
            [len(ports), len(frequencies), len(gains), repeat_count]))

    all_gain_results_dataspace = context_stack.enter_context(
        HDF5Dataspace.create_simple(
            [len(ports), len(frequencies_subset), len(all_gains),
             repeat_count]))

    main_results_nf_dataset = context_stack.enter_context(
        h5_file.create_dataset("nf_all_frequency_dataset",
                               HDF5TypeBuiltin.IEEE_F64LE,
                               main_results_dataspace))
    main_results_noise_power_on_dataset = context_stack.enter_context(
        h5_file.create_dataset("noise_power_on_all_frequency_dataset",
                               HDF5TypeBuiltin.IEEE_F64LE,
                               main_results_dataspace))
    main_results_noise_power_off_dataset = context_stack.enter_context(
        h5_file.create_dataset("noise_power_off_all_frequency_dataset",
                               HDF5TypeBuiltin.IEEE_F64LE,
                               main_results_dataspace))
    main_results_nf_data = main_results_nf_dataset.make_data()
    main_results_noise_power_on_data = \
            main_results_noise_power_on_dataset.make_data()
    main_results_noise_power_off_data = \
            main_results_noise_power_off_dataset.make_data()

    all_gain_nf_dataset = context_stack.enter_context(
        h5_file.create_dataset("nf_all_gain_dataset",
                               HDF5TypeBuiltin.IEEE_F64LE,
                               all_gain_results_dataspace))
    all_gain_noise_power_on_dataset = context_stack.enter_context(
        h5_file.create_dataset("noise_power_on_all_gain_dataset",
                               HDF5TypeBuiltin.IEEE_F64LE,
                               all_gain_results_dataspace))
    all_gain_noise_power_off_dataset = context_stack.enter_context(
        h5_file.create_dataset("noise_power_off_all_gain_dataset",
                               HDF5TypeBuiltin.IEEE_F64LE,
                               all_gain_results_dataspace))
    all_gain_nf_data = all_gain_nf_dataset.make_data()
    all_gain_noise_power_on_data = \
            all_gain_noise_power_on_dataset.make_data()
    all_gain_noise_power_off_data = \
            all_gain_noise_power_off_dataset.make_data()


    start = time.time()
    last = time.time()

    for channel, antenna, port_name in ports:
        result = 'n'
        print("Time since last: %f"%(time.time()-last))
        a_thread = alert_thread.AlertThread()
        a_thread.start()
        while result != 'y':
            result = input('Connect noise source to port %s? [y/n]: '%
                           (port_name))
            if result == 'y':
                tune_req = uhd.types.TuneRequest(2.4e9)
                for i in range(2):
                    device.set_rx_freq(tune_req, i)
                    device.set_rx_gain(76, i)
                    device.set_rx_antenna(antenna, i)
                noise_figures, noise_power_off_values, noise_power_on_values = \
                        measure_noise(2.4e9, channel, 1)
                if noise_figures[0] <= 0 or noise_figures[0] > 10:
                    print("...this isn't telepathy, plug it in and turn it on...")
                    result = 'n'

        a_thread.running = False
        a_thread.join()

        last = time.time()

        for frequency_idx, freq in enumerate(frequencies):
            print("Frequency:", freq)

            tune_req = uhd.types.TuneRequest(freq)
            for i in range(2):
                device.set_rx_freq(tune_req, i)

            for gain_idx, gain in enumerate(gains):
                for i in range(2):
                    device.set_rx_gain(gain, i)

                for i in range(2):
                    device.set_rx_antenna(antenna, i)

                noise_figures, noise_power_off_values, noise_power_on_values = \
                        measure_noise(freq, channel, repeat_count)

                for j in range(repeat_count):
                    dataset_idx = (portmap[(channel, antenna)],
                                   frequency_idx, gain_idx, j)
                    main_results_nf_data[dataset_idx] = noise_figures[j]
                    main_results_noise_power_on_data[dataset_idx] = \
                            noise_power_on_values[j]
                    main_results_noise_power_off_data[dataset_idx] = \
                            noise_power_off_values[j]

        main_results_nf_dataset[...] = main_results_nf_data
        main_results_noise_power_on_dataset[...] = \
                main_results_noise_power_on_data
        main_results_noise_power_off_dataset[...] = \
                main_results_noise_power_off_data


        for frequency_idx, freq in enumerate(frequencies_subset):
            print("Frequency:", freq)

            tune_req = uhd.types.TuneRequest(freq)
            for i in range(2):
                device.set_rx_freq(tune_req, i)

            for gain_idx, gain in enumerate(all_gains):
                for i in range(2):
                    device.set_rx_gain(gain, i)

                for i in range(2):
                    device.set_rx_antenna(antenna, i)

                noise_figures, noise_power_off_values, noise_power_on_values = \
                        measure_noise(freq, channel, repeat_count)

                for j in range(repeat_count):
                    dataset_idx = (portmap[(channel, antenna)],
                                   frequency_idx, gain_idx, j)
                    all_gain_nf_data[dataset_idx] = noise_figures[j]
                    all_gain_noise_power_on_data[dataset_idx] = \
                            noise_power_on_values[j]
                    all_gain_noise_power_off_data[dataset_idx] = \
                            noise_power_off_values[j]

        all_gain_nf_dataset[...] = all_gain_nf_data
        all_gain_noise_power_on_dataset[...] = \
                all_gain_noise_power_on_data
        all_gain_noise_power_off_dataset[...] = \
                all_gain_noise_power_off_data
