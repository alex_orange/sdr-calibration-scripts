#!/bin/bash

virtualenv -p python3.6 ve

ln -s /usr/lib64/python3.6/site-packages/SoapySDR.py ve/lib64/python3.6/site-packages/
ln -s /usr/lib64/python3.6/site-packages/_SoapySDR.so ve/lib64/python3.6/site-packages/
ln -s /usr/lib64/python3.6/site-packages/uhd ve/lib64/python3.6/site-packages/


. ve/bin/activate

pip install ~/LDB_HDF5
pip install numpy
